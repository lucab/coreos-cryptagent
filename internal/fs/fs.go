// Copyright 2018 CoreOS, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package fs

import (
	"io/ioutil"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"github.com/pkg/errors"
)

// LookupBlockdev translates a block device path into its `/dev/block` entry
//
// `path` must be an existing absolute path. The resulting string is an absolute
// path rooted at `/dev/block/`.
func LookupBlockdev(pathIn string) (string, error) {
	var err error
	realPath, err := filepath.EvalSymlinks(pathIn)
	if err != nil {
		return "", errors.Wrapf(err, "resolving %q", pathIn)
	}

	cache := map[string]string{}
	fis, err := ioutil.ReadDir("/dev/block/")
	if err != nil {
		return "", errors.Wrap(err, "failed to list /dev/block")
	}
	for _, fi := range fis {
		entry := filepath.Join("/dev/block", fi.Name())
		logrus.Debugf("resolving %q", entry)
		if resolved, err := filepath.EvalSymlinks(entry); err == nil {
			cache[resolved] = entry
			logrus.Debugf("found %q from %q", resolved, entry)
		}
	}

	blockPath, ok := cache[realPath]
	if ok && blockPath != "" {
		return blockPath, nil
	}

	return "", errors.Errorf("unable to lookup %q", realPath)
}
